/* You should implement your request handler function in this file.
 * And hey! This is already getting passed to http.createServer()
 * in basic-server.js. But it won't work as is.
 * You'll have to figure out a way to export this function from
 * this file and include it in basic-server.js so that it actually works.
 * *Hint* Check out the node module documentation at http://nodejs.org/api/modules.html. */
var _ = require('underscore');
var url = require('url');

var chats = {};
chats.storage = [
  {
    "objectId"  : "0",
    "createdAt" : "2014-05-06T10:32:07.054Z",
    "updatedAt" : "2014-05-06T10:32:07.054Z",
    "roomname"  : "lobby",
    "text"      : "This is the 1st message",
    "username"  : "imayolas"
  },

  {
    "objectId"  : "1",
    "createdAt" : "2014-05-06T10:33:07.054Z",
    "updatedAt" : "2014-05-06T10:33:07.054Z",
    "roomname"  : "lobby",
    "text"      : "This is the 2nd message",
    "username"  : "imayolas"
  },

  {
    "objectId"  : "2",
    "createdAt" : "2014-05-06T10:33:09.054Z",
    "updatedAt" : "2014-05-06T10:33:09.054Z",
    "roomname"  : "room1",
    "text"      : "This is the 3rd message",
    "username"  : "c99"
  }

];

chats.objectId = 4;

chats.addMessage = function(msg, room){

  var fullMessage = {};
  var currentDateTime = new Date();

  fullMessage.objectId = chats.objectId;
  fullMessage.createdAt = currentDateTime.toISOString();
  fullMessage.updatedAt = currentDateTime.toISOString();
  fullMessage.roomname = room;
  fullMessage.username = msg.username;
  fullMessage.text = msg.text;

  chats.storage.push(fullMessage);
  chats.objectId++;

  return chats.objectId - 1;
}

chats.getMessages = function(room, lastId){

  var chatsObj = {};
  chatsObj.results = [];

  if(room !== 'messages'){


    _.each(chats.storage, function(message){
      if(message.roomname === room && message.objectId > lastId){
        chatsObj.results.push(message);
      }
    });

  } else {

    _.each(chats.storage, function(message){
      if(message.objectId > lastId){
        chatsObj.results.push(message);
      }
    });

  }

  chatsObj.results = _.sortBy(chatsObj.results, function(msg){ return -msg.objectId});

  return JSON.stringify(chatsObj);

}



var handlerRequest = function(request, response) {

  console.log("Serving request type " + request.method + " for url " + request.url);


  // Define headers
  var headers = defaultCorsHeaders;
  headers['Content-Type'] = "text/plain";

  // Get requestUrl power-up
  var requestUrl = url.parse(request.url, true);
  requestUrl.subPaths = requestUrl.pathname.split("/");
  requestUrl.subPaths.shift();

  // Define end404()
  var end404 = function(){
    response.writeHead(404, "Not found", headers);
    response.end();
  }

  // Respond to OPTIONS
  if(request.method === "OPTIONS"){

    response.writeHead(200, headers);
    response.end();


  } else if ( request.method === "GET" ){

    if(requestUrl.subPaths[0] === "classes"){

      var lastId = requestUrl.query.lastId || -1;

      headers['Content-Type'] = "application/json";
      var room = requestUrl.subPaths[1];
      response.writeHead(200, headers);
      response.end(chats.getMessages(room, lastId));

    } else {
      end404();
    }


  } else if ( request.method === "POST" ){

    if(requestUrl.subPaths[0] === "classes"){

      var room = requestUrl.subPaths[1];

      var body = '';
      request.setEncoding('utf8');
      response.writeHead(201, headers);

      request.on('data', function(chunk){
        body += chunk;
      });

      request.on('end', function(){
        var lastId = chats.addMessage(JSON.parse(body), room);
        headers['Content-Type'] = "application/json";
        response.end(JSON.stringify({results: {lastId: lastId}}));
      });

    } else {

      end404();

    }
  } else {

    end404();

  }

};


var defaultCorsHeaders = {
  "Access-Control-Allow-Origin": "*",
  "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, OPTIONS",
  "Access-Control-Allow-Headers": "content-type, accept, X-Parse-Application-Id, X-Parse-REST-API-Key",
  "Access-Control-Max-Age": 10 // Seconds.
};

module.exports = handlerRequest;
